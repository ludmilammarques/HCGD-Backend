# Setup Linux

## Database

Here, we used LAMPP to have an interface to manage the MySQL, but you can install other of your choice.

### Install LAMPP
https://www.edivaldobrito.com.br/como-instalar-o-xampp-no-linux/

### Start LAMPP
`sudo /opt/lampp/lampp start`

And access http://localhost/phpmyadmin

### Create the database
- On phpMyAdmin you should create a new database called `hcgd` 
- Into `hcgd`, create a user with the same credentials of `settings.py` on DATABASE section

## Python Setup

### Install PIP (Python Installs Packages)
`sudo apt-get install python-pip`

Then enter into HCGD-Backend folder and execute the following commands:

### Create a virtualenv to isolate our package dependencies locally
`virtualenv env`

`source env/bin/activate`  

### Install Django and Django REST framework into the virtualenv
`pip install django`  

`pip install djangorestframework`

### Install MySQL
`pip install mysqlclient`

### Install CoreAPI for documentation  
`pip install coreapi`

### Install JWT
`pip install djangorestframework-jwt`

### Install CORS
`pip install django-cors-headers`

## Running the project
`source env/bin/activate`  

`cd hcgd`

Now sync your database for the *first time* and whenever you make a change on models: </br>
`python manage.py migrate`

Running the local server: </br>
`python manage.py runserver`